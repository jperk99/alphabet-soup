#Execute in Python 3.7.x. Should not require any PIP Installs.
import sys
from Wordsearch import wordsearch as ws
def main():
    fileName = sys.argv[1]
    WS = ws(fileName)
    WS.search()
    print(WS)

main()