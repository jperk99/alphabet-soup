#This is a class that holds the array for a word search, the words to find, and the location of the words as they are found
#Known Bugs: This will mark all occurences of a word. So if you have a situation where the word bear is used, and the combo word polar bear is used
#bear will be marked twice.
class wordsearch:
    
    #Initialize:
        #wordMap : A dictionary consisting of { tuple(tuple(x1,y1),tuple(x2,y2)) : string Word}
        #wordList : A list consisting of the strings given to find. Words will be removed from this array as they are found.
        #wordBLock : A list of lists holding the word search text block.
        #rows : The number of rows in the wordBlock.
        #columns : the number of columns in the wordBlock.
    #Params: fileName should be the exact location of the text file.

    def __init__(self, fileName):

        inFile = None
        try: 
            inFile = open(fileName, 'r')
        except OSError:
            print("Could not open/read file at location: ", fileName)
            SystemExit()
        
        with inFile:
            RnC = inFile.readline()
            RnC = RnC.split('x')
            self.columns = int(RnC[1])
            self.rows = int(RnC[0])
            
            self.wordBlock = list()
            for i in range(0,self.rows):
                tempBlock = inFile.readline()
                #Remove any leading and trailing whitespace, they would mess up our string searcher later.
                tempBlock = tempBlock.strip()
                #split the characters up by whitespace.
                tempBlock = tempBlock.split(' ')
                #add this line to the wordBlock
                self.wordBlock.append(tempBlock)

            self.wordList = list()
            tempList = inFile.readlines()
            for word in tempList:
                self.wordList.append(word.strip())

            self.wordMap = {}

            inFile.close()
    def __str__(self):
        toString = ""
        items = self.wordMap.items()
        for item in items:
            #triple indexed (lol) because tuple for item, tuple for set of coords, tuple for x,y pair.
            toString += item[1] + " " + str(item[0][0][0]) + ":" + str(item[0][0][1]) + " " + str(item[0][1][0]) + ":" + str(item[0][1][1]) + "\n"
        return toString

    #Search performs 4 kinds of searches. Row, Column, Diagonal (top right to bottom left), and Diagonal (top left to bottom right)
    def search(self):
        #First, Row Wise search.
        #Build rowBlock, a row Wise iterable to compare the words against.
        rowBlock = list()
        for row in self.wordBlock:
            tempString = ""
            for c in row:
                tempString += c
            rowBlock.append(tempString)

        rowCoord = 0
        for row in rowBlock:
            
            #Search the current Row for every word we are looking for, forwards and backwards.
            for word in self.wordList:
                #replace spaces with an empty character to remove them, then search for the word(s).
                columnCoord = row.find(word.replace(' ', ''))
                if columnCoord >= 0:
                    
                    self.wordMap[((rowCoord,columnCoord),(rowCoord, columnCoord + len(word) - 1))] = word
                else:
                    rWord = word[::-1] #This line reverses the current word we are looking for.
                    columnCoord = row.find(rWord.replace(' ', ''))
                    if columnCoord >= 0:
                        
                        self.wordMap[((rowCoord, columnCoord + len(word) - 1), (rowCoord,columnCoord))] = word

            #increment the rowCoord to keep track of which row this is.
            rowCoord += 1

        #Second, Column Wise Search.
        columnBlock  = list()
        for column in range(0,self.columns):
            tempString = ""
            for row in range(0,self.rows):
                tempString += self.wordBlock[row][column]
            columnBlock.append(tempString)
        
        columnCoord = 0
        for column in columnBlock:
            
            #Search the current Row for every word we are looking for, forwards and backwards.
            for word in self.wordList:
                #replace spaces with an empty character to remove them, then search for the word(s).
                rowCoord = column.find(word.replace(' ', ''))
                if rowCoord >= 0:
                    
                    self.wordMap[((rowCoord,columnCoord), (rowCoord + len(word) - 1, columnCoord))] = word
                else:
                    rWord = word[::-1] #This line reverses the current word we are looking for.
                    rowCoord = column.find(rWord.replace(' ', ''))
                    if rowCoord >= 0:
                        
                        self.wordMap[((rowCoord + len(word) - 1, columnCoord), (rowCoord,columnCoord))] = word


            columnCoord += 1

        #Third, Diagonal( Top right to Bottom left)
        diagRLBlock = list()  
        startRow = 0
        startColumn = 0
        # while the starting row for string building is not the total number of rows and starting column is not the last index of the columns.
        while(startRow != self.rows or startColumn != self.columns - 1):
            #prep for iteration
            rowCoord = startRow
            columnCoord = startColumn
            tempString = ""

            #iterate until we are out of the word block on the left side (columnCoord < 0) or the bottom (row coord >= self.rows)
            while rowCoord < self.rows and columnCoord >= 0:
                tempString += self.wordBlock[rowCoord][columnCoord]
                rowCoord += 1
                columnCoord -= 1

            diagRLBlock.append(tempString)
            #increment startRow if startColumn is at the farthest possible, otherwise increment startColumn.
            if startColumn < self.columns - 1:
                startColumn += 1
            else:
                startRow += 1

        startRow = 0
        startColumn = 0
        for diag in diagRLBlock:
            
            #Search the current Row for every word we are looking for, forwards and backwards.
            for word in self.wordList:
                #replace spaces with an empty character to remove them, then search for the word(s).
                positionInDiag = diag.find(word.replace(' ', ''))
                if positionInDiag >= 0:
                    
                    wordStart = (startRow + positionInDiag, startColumn - positionInDiag)
                    self.wordMap[(wordStart , (wordStart[0] + len(word) - 1, wordStart[1] - len(word) + 1))] = word
                else:
                    rWord = word[::-1] #This line reverses the current word we are looking for.
                    positionInDiag = diag.find(rWord.replace(' ', ''))
                    if positionInDiag >= 0:
                        
                        wordStart = (startRow + positionInDiag, startColumn - positionInDiag)
                        self.wordMap[((wordStart[0] + len(word) - 1, wordStart[1] - len(word) + 1), wordStart)] = word


            if startColumn < self.columns - 1:
                startColumn += 1
            else:
                startRow += 1

       #Fourth, Diagonal( Top left to Bottom right)
        diagLRBlock = list()  
        startRow = 0
        startColumn = self.columns - 1
        # while the starting row for string building is not the total number of rows and starting column is not the last index of the columns.
        while(startRow != self.rows or startColumn != 0):
            #prep for iteration
            rowCoord = startRow
            columnCoord = startColumn
            tempString = ""

            #iterate until we are out of the word block on the left side (columnCoord < 0) or the bottom (row coord >= self.rows)
            while rowCoord < self.rows and columnCoord < self.columns:
                tempString += self.wordBlock[rowCoord][columnCoord]
                rowCoord += 1
                columnCoord += 1

            diagLRBlock.append(tempString)
            #increment startRow if startColumn is at the farthest possible, otherwise increment startColumn.
            if startColumn > 0:
                startColumn -= 1
            else:
                startRow += 1

        startRow = 0
        startColumn = self.columns - 1

        for diag in diagLRBlock:
            
            #Search the current Row for every word we are looking for, forwards and backwards.
            for word in self.wordList:
                #replace spaces with an empty character to remove them, then search for the word(s).
                positionInDiag = diag.find(word.replace(' ', ''))
                if positionInDiag >= 0:
                    wordStart = (startRow + positionInDiag, startColumn + positionInDiag)
                    self.wordMap[(wordStart, (wordStart[0] + len(word) - 1, wordStart[1] + len(word) - 1))] = word
                else:
                    rWord = word[::-1] #This line reverses the current word we are looking for.
                    positionInDiag = diag.find(rWord.replace(' ', ''))
                    if positionInDiag >= 0:
                        
                        wordStart = (startRow + positionInDiag, startColumn + positionInDiag)
                        self.wordMap[((wordStart[0] + len(word) - 1, wordStart[1] + len(word) - 1), wordStart)] = word


            if startColumn > 0:
                startColumn -= 1
            else:
                startRow += 1

        foundWords = list(self.wordMap.values())
        for word in self.wordList:
            if foundWords.count(word) == 0:
                print(word + " Wasn't Found!")
